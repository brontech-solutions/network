variable "AWS_Region" {}

variable "vpc_cidr" {
    description = "vpc_cidrs for vpc and both subnets"
    type = list(object({
        vpc_cidr = string
        name = string
    }))
}
variable "SN_AZ" {
    description = "This is for both public and private SN AZ"
    type = list(object({
        SN_AZ = string
        name = string
    }))
}
