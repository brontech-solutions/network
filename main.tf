
resource "aws_vpc" "My-vpc" {
  cidr_block = var.vpc_cidr[0].vpc_cidr
  tags = {
    Name: var.vpc_cidr[0].name
    vpc_env: "dev"
  }
}

resource "aws_subnet" "Pub_SN" {
  vpc_id = aws_vpc.My-vpc.id
  cidr_block = var.vpc_cidr[1].vpc_cidr
  availability_zone = var.SN_AZ[0].SN_AZ

  tags = {
    Name: var.vpc_cidr[1].name
  }
}

resource "aws_subnet" "Priv-SN" {
  vpc_id = aws_vpc.My-vpc.id
  cidr_block = var.vpc_cidr[2].vpc_cidr
  availability_zone = var.SN_AZ[1].SN_AZ

  tags = {
    Name: var.vpc_cidr[2].name
  }
}

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.My-vpc.id

  tags = {
    Name: "Dev-IGW"
  }
}

resource "aws_route_table" "Pub_RT" {
  vpc_id = aws_vpc.My-vpc.id
  
  route  {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW.id
  }
  
}
resource "aws_route_table_association" "Pub_RT_Ass" {
  subnet_id = aws_subnet.Pub_SN.id
  route_table_id = aws_route_table.Pub_RT.id
  
}

  output "Dev-vpc" {
    value = aws_vpc.My-vpc.id
  }
  output "Pub_SN" {
    value = aws_subnet.Pub_SN.id
  }



